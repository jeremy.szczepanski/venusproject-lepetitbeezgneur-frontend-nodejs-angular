// import { Component, OnInit } from '@angular/core';
// import { Paddle } from 'src/app/models/';
// import { PaddlesService } from '../../services/';
// /*** ANGULAR FULL CALENDAR ***/
// import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking (imports must go before any of the FullCalendar plugins )

// @Component({
//   selector: 'app-paddle',
//   templateUrl: './paddle.component.html',
//   styleUrls: ['./paddle.component.scss']
// })
// export class PaddleComponent implements OnInit {

//   calendarOptions: CalendarOptions = {
//     initialView: 'dayGridMonth'
//   };


//   constructor(private paddleService: PaddlesService, private router: Router) { }

//   ngOnInit(): void {
//     this.paddleService.getAll().subscribe(paddles => 
//       {
//         this.paddleList = paddles;
//       });
//   }