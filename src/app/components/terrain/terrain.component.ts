import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Terrain } from 'src/app/models/terrain.model';
import { TerrainsService } from '../../services/terrain.service';
/*** ANGULAR FULL CALENDAR ***/
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking (imports must go before any of the FullCalendar plugins )

@Component({
  selector: 'app-terrains',
  templateUrl: './terrain.component.html',
  styleUrls: ['./terrain.component.scss']
})
export class TerrainsComponent implements OnInit {
  public terrainsList!: Terrain[];

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth'
  };


  constructor(private terrainService: TerrainsService, private router: Router) { }

  ngOnInit(): void {
    this.terrainService.getAll().subscribe(terrains => 
      {
        this.terrainsList = terrains;
      });
  }

  // edit(Id_Terrain: any): void
  // {
  //   this.router.navigate(['edit_terrain/'+Id_Terrain]);
  // }

  // delete(terrain: Terrain): void
  // {
  //   if(confirm("are you sure ?!!!!"))
  //   {
  //     this.terrainService.deleteTerrain(terrain).subscribe(terrains => 
  //       {
  //         this.terrainsList = terrains;
  //       });
  //   }
  // }
}
