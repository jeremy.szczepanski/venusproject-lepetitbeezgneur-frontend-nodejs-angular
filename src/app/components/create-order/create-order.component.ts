import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, NgForm   } from '@angular/forms';
import { Router } from '@angular/router';
import { ReservationService } from 'src/app/services/reservation.service';
import { Commande} from 'src/app/models/commande.model';
import { Terrain } from 'src/app/models/terrain.model';


@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})



export class CreateOrderComponent implements OnInit {
  commandeForm!: FormGroup;
  Id_CommandeCtl!: FormControl;
  Id_TerrainCtl!: FormControl;
  start_atCtl!: FormControl;
  end_atCtl!: FormControl;
  
  commandeList!: Commande[];
  terrainList!: Terrain[];

  constructor(private reservationService: ReservationService, 
    private reservationTerrain: ReservationService,
    private formBuilder: FormBuilder, 
    private router: Router ) { 
    this.initForm();
    }
    
  ngOnInit(): void {
      this.reservationService.getAll().subscribe(commandes => 
        {
          this.commandeList = commandes;
        });
      }
      
  initForm(): void
      {
        this.Id_CommandeCtl = this.formBuilder.control('', [Validators.required]);
        this.Id_TerrainCtl = this.formBuilder.control('', [Validators.required]);
        this.start_atCtl = this.formBuilder.control('', [Validators.required]);
        this.end_atCtl = this.formBuilder.control('', [Validators.required]);
        
    
        this.commandeForm = this.formBuilder.group({
          Id_Commande : this.Id_CommandeCtl,
          Id_Terrain : this.Id_TerrainCtl,
          start_at : this.start_atCtl,
          end_at : this.end_atCtl,

        });
      }


  onSubmit()
  {
    const formVal = this.commandeForm.value;

    formVal.id = 0;
    const newCommande = new Commande(formVal);
    this.reservationService.addReservation(newCommande).subscribe(m =>{});

    this.router.navigate(['/reservation']);
  }

  resetCommandeForm(commandeForm:NgForm) {
    commandeForm.resetForm();
  }

}
