import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UsersCommonService } from 'src/app/services/user-common.service';
import { UsersService } from '../../services/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})

export class CreateUserComponent implements OnInit {
  userForm!: FormGroup;
  usernameCtl!: FormControl;
  emailCtl!: FormControl;
  passwordCtl!: FormControl;
  passwordConfirmCtl!: FormControl;
  isNew: boolean = true;
  user!: User;
  first_nameCtl!: FormControl;
  last_nameCtl!: FormControl;
  adresse_rueCtl!: FormControl;
  adresse_nbrCtl!: FormControl;
  adresse_cpCtl!: FormControl;
  adresse_villeCtl!: FormControl;

  constructor(private userService: UsersService, 
    private usersCommonService: UsersCommonService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private formBuilder: FormBuilder,
    private authService: AuthService)   {
    this.initForm();
  }

  ngOnInit(): void {
    if(this.route.snapshot.params["id"])
    {
      this.userService.getOneByID(this.route.snapshot.params["id"]).subscribe(m => 
        {
          if(m)
          {   
            this.isNew = false;
            this.user = m;
            this.userForm.patchValue(this.user);
          }
        });
    }
  }

  initForm(): void
  {
    this.usernameCtl = this.formBuilder.control('', [Validators.required], [this.usernameExist()]);
    this.emailCtl = this.formBuilder.control('', [Validators.required, Validators.email]);
    this.passwordCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(6), this.checkPassword()]);
    this.passwordConfirmCtl = this.formBuilder.control('', [Validators.required, this.checkConfirm()]);

    this.first_nameCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(1)]);
    this.last_nameCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(1)]);
    this.adresse_rueCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(1)]);
    this.adresse_nbrCtl = this.formBuilder.control('');
    this.adresse_cpCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(1)]);
    this.adresse_villeCtl = this.formBuilder.control('', [Validators.required, Validators.minLength(1)]);


    this.userForm = this.formBuilder.group({
      username: this.usernameCtl,
      email: this.emailCtl,
      password: this.passwordCtl,
      passwordConfirm: this.passwordConfirmCtl,
      first_name: this.first_nameCtl,
      last_name: this.last_nameCtl,
      adresse_nbr: this.adresse_rueCtl,
      adresse_rue: this.adresse_nbrCtl,
      adresse_cp: this.adresse_cpCtl,
      adresse_ville: this.adresse_villeCtl

    });
  }

  usernameExist(): any
  {
    var timeout: any;
    return (ctl: FormControl) =>
    {
      clearTimeout(timeout);
      const username = ctl.value;
      return new Promise(resolve => {
        timeout = setTimeout(() =>{
          if(ctl.pristine)
          {
            resolve(null);
          } else 
          {
            this.usersCommonService.getOneByName(username).subscribe(user => 
              {
                resolve(user && this.isNew ? { usernameExist: true } : null);
              })
          }
        }, 300)
      });
    }
  }

  checkPassword(): ValidatorFn
  {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const password = control.value;
      
      let hasNumber = /\d/;
      let hasErrors = false;
      let errors = {forbidden: {value: 'passwords and user name are equal'}, pwdAndUsernameEqual: false, pwdMustContainNbr: false, 
        pwdRegEx: false };
      var pattern = new RegExp(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$"
      );

      if(password === this.usernameCtl.value && (!control.hasError('required')))
      {
        hasErrors = true;
        errors.pwdAndUsernameEqual = true;
      } else if(!hasNumber.test(password))
      {
        hasErrors = true;
        errors.pwdMustContainNbr = true; 
      } else if(!pattern.test(password))
      {
        hasErrors = true;
        errors.pwdRegEx = true;
      }
      return hasErrors ? errors : null;
    };
  }

  checkConfirm(): ValidatorFn
  {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const password = control.value;

      if(password != this.passwordCtl.value && (!control.hasError('required')))
      {
        return  {forbidden: {value: 'passwords are not the same'}, pwdNotEqual: true };
      }
      return null;
    };
  }

  onSubmit()
  {
    const formVal = this.userForm.value;

    formVal.id = 0;
    const newUser = new User(formVal);
    this.usersCommonService.addUser(newUser).subscribe(m => {});
    
    //this.router.navigate(['/create']);
    this.router.navigate(['/user/'+this.authService.getCurrentUser().Id_Users]);// on se logue automatiquement après la création d'un user]);
  }

}
