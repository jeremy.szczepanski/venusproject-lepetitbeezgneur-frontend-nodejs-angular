import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UsersService } from '../../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UsersComponent implements OnInit {
  public usersList!: User[];

  constructor(private userService: UsersService, private router: Router) { }

  ngOnInit(): void {
    this.userService.getAll().subscribe(users => 
      {
        this.usersList = users;
      });
  }

  edit(Id_Users: any): void
  {
    this.router.navigate(['edit_user/'+Id_Users]);
  }

  delete(user: User): void
  {
    if(confirm("are you sure ?!!!!"))
    {
      this.userService.deleteUser(user).subscribe(users => 
        {
          this.usersList = users;
        });
    }
  }

}
