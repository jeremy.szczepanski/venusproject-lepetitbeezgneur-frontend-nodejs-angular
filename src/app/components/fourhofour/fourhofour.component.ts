import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-fourhofour',
  templateUrl: './fourhofour.component.html',
  styleUrls: ['./fourhofour.component.scss']
})
export class FourhofourComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public example()
  {
    this.pipe_1().subscribe(m => 
      {
        console.log(m);
      });
  }

  private pipe_1()
  {
    return this.pipe_2().pipe(
      map(m => { 
        console.log(m)
        m.username = "new username"
        return m;
      })
    )
  }

  private pipe_2()
  {
    return this.pipe_3().pipe(
      map(m => {
        console.log(m);
        return new User(m)
      })
    );
  }

  private pipe_3()
  {
    return of({
      username: "test",
      password: "test",
      testVar: "this won't be prompt after pipe 2",
      email: "test@test",
      id: 1
    });
  }

}
