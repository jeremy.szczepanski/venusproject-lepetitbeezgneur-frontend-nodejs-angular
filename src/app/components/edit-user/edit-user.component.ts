import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UsersCommonService } from 'src/app/services/user-common.service';
import { UsersService } from '../../services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  userForm!: FormGroup;
  usernameCtl!: FormControl;
  emailCtl!: FormControl;
  passwordCtl!: FormControl;
  isNew: boolean = true;
  user!: User;

  constructor(private userService: UsersService, 
    private usersCommonService: UsersCommonService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private formBuilder: FormBuilder,
    private authService: AuthService)   {
    this.initForm();
  }

  ngOnInit(): void {
    if(this.route.snapshot.params["id"])
    {
      this.userService.getOneByID(this.route.snapshot.params["id"]).subscribe(m => 
        {
          if(m)
          {   
            this.isNew = false;
            this.user = m;
            this.userForm.patchValue(this.user);
          }
        });
    }
  }

  initForm(): void
  {
    this.usernameCtl = this.formBuilder.control('', [Validators.required], [this.usernameExist()]);
    this.emailCtl = this.formBuilder.control('', [Validators.required, Validators.email]);
    this.passwordCtl = this.formBuilder.control('', [Validators.required]);

    this.userForm = this.formBuilder.group({
      username: this.usernameCtl,
      email: this.emailCtl,
      password: this.passwordCtl
    });
  }

  usernameExist(): any
  {
    var timeout: any;
    return (ctl: FormControl) =>
    {
      clearTimeout(timeout);
      const username = ctl.value;
      return new Promise(resolve => {
        timeout = setTimeout(() =>{
          if(ctl.pristine)
          {
            resolve(null);
          } else 
          {
            this.usersCommonService.getOneByName(username).subscribe(user => 
              {
                resolve(user && this.isNew ? { usernameExist: true } : null);
              })
          }
        }, 300)
      });
    }
  }

  onSubmit()
  {
    const formVal = this.userForm.value;
    
    if(this.isNew)
    {
      formVal.id = 0;
      const newUser = new User(formVal);
      this.usersCommonService.addUser(newUser).subscribe(m => {});
    } else 
    {
      formVal.id = this.user.Id_Users;
      const newUser = new User(formVal);
      this.userService.updateUser(newUser).subscribe(m => 
        {
          if(m)
          {
            if(this.user.username == this.authService.getCurrentUser().username)
            {
              this.authService.updateCurrentUser(newUser);
            }
          }
        });
    }
    
    this.router.navigate(['/users']);
  }

}
