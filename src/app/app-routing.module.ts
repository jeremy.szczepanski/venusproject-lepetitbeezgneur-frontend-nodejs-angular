import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AuthComponent } from './components/auth/auth.component';
import { CreateOrderComponent } from './components/create-order/create-order.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { FourhofourComponent } from './components/fourhofour/fourhofour.component'
import { MainComponent } from './components/main/main.component';
import { TerrainsComponent } from './components/terrain/terrain.component';
import { CommentaireComponent } from './components/commentaire/commentaire.component';
import { ArticleComponent } from './components/article/article.component';
//import { CreateCommandeComponent } from './components/commande/commande.component';
//import { ProfileComponent } from './components/mat/profile/profile.component';
//import { UsersContainerComponent } from './components/users-container/users-container.component';
import { UsersComponent } from './components/user/user.component';
import { AdminGuard, AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'auth', component: AuthComponent },
  { path: 'create_user', component: CreateUserComponent },
  { path: 'user', component: UsersComponent },  //affiche la liste des Users pour modification administratives.
  { path: 'terrains', component: TerrainsComponent }, //affiche la liste des terrains pour modification administratives.
  { path: 'reservation', component: CreateOrderComponent }, //envoie les données de formulaire de commande dans le back avec création d'Id_Commande
  { path: 'commentaire', component: CommentaireComponent }, //TODO
  { path: 'article', component: ArticleComponent }, //TODO



  { path: '', canActivate: [AuthGuard], children: [
    //{ path: 'user/:id', component: ProfileComponent },
    { path: '', canActivate: [AdminGuard], children: [
      //{ path: 'users', component: UsersContainerComponent },
      { path: 'edit_user/:id', component: EditUserComponent },
    ]},
  ]},
  { path: 'home', component: MainComponent },
  { path: 'not-found', component: FourhofourComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
