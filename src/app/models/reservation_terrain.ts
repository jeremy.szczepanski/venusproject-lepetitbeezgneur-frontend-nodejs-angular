export class ReservationTerrain
{
    Id_Commande: number;
    Id_Terrain: number;
    date_reservation: Date;
    start_at: Date;
    end_at: Date;
    nom_terrain: string;
    Prix_heure: number;


    constructor(data: any)
    {
        this.Id_Commande = data.Id_Commande;
        this.Id_Terrain = data.Id_terrain;
        this.date_reservation = data.date_reservation;
        this.start_at = data.start_at;
        this.end_at = data.end_at;
        this.nom_terrain = data.nom_terrain;
        this.Prix_heure = data.Prix_heure;
    }
}