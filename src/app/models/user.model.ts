export class User
{
    Id_Users: number;
    is_verified:number;
    username: string;
    first_name: string;
    last_name: string;
    adresse_rue: string;
    adresse_nbr: string;
    adresse_cp: string;
    adresse_ville: string;
    email: string;
    admin: number;
    password: string;

    constructor(data: any)
    {
        this.Id_Users = data.Id_Users;
        this.is_verified = data.is_verified;
        this.username = data.username;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.adresse_rue = data.adresse_rue;
        this.adresse_nbr = data.adresse_nbr;
        this.adresse_cp = data.adresse_cp;
        this.adresse_ville = data.adresse_ville;
        this.email = data.email;
        this.admin = data.admin ? data.admin : 0;
        this.password = data.password;

    }
}