export class Terrain
{
    Id_Terrain: number;
    start_at: Date;
    end_at: Date;
    nom_terrain: string;
    Prix_heure: number;

    constructor(data: any)
    {
        this.Id_Terrain = data.Id_Terrain;
        this.start_at = data.start_at;
        this.end_at = data.end_at;
        this.nom_terrain = data.nom_terrain;
        this.Prix_heure = data.Prix_heure;
    }
}