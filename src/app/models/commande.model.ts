export class Commande
{
    Id_Commande: number;
    DateCommnande: Date;
    Montant: number;
    Commande_Honoree: boolean;
    Id_Client: number;

    constructor(data: any)
    {
        this.Id_Commande = data.Id_Commande;
        this.DateCommnande = data.DateCommnande;
        this.Montant = data.Montant;
        this.Commande_Honoree = data.Commande_Honoree;
        this.Id_Client = data.Id_Client;
    }
}