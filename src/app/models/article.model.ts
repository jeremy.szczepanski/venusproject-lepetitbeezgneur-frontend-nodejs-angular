export class Article
{
    Id_Articles: number;
    titre: string;
    slug: string;
    contenu: string;
    created_at: Date;
    updated_at: Date;
    featured_image: ImageData;
    Id_employee: number;

    constructor(data: any)
    {
        this.Id_Articles = data.Id_Articles;
        this.titre = data.titre;
        this.slug = data.slug;
        this.contenu = data.contenu;
        this.created_at = data.created_at;
        this.updated_at = data.updated_at;
        this.featured_image = data.featured_image;
        this.Id_employee = data.Id_employee;
    }
}