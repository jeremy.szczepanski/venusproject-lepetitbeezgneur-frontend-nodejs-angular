import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Le Petit Beezgneur';
  //paddleLogo : string ='../assets/Image_Bank/Home/Paddle_logo-1024x767.jpg';



  constructor(private authService: AuthService, private router: Router)
  {
  }

  public checkAuth()
  {
    return this.authService.isLoggedIn;
  }

  public logout(): void
  {
    this.authService.logout();
  }

  public profile(): void 
  {
    const profileRte = 'users/' + this.authService.getCurrentUser().Id_Users;
    this.router.navigate([profileRte]);
  }

}