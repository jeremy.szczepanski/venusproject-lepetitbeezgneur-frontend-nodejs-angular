import { Injectable } from '@angular/core';
import { ServerService } from './server.service';
import { Terrain } from '../models/terrain.model';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TerrainsService {

  constructor(private server: ServerService) 
  {}

  public getAll(): Observable<Terrain[]> 
  {
    return this.server.get<Terrain[]>('terrains/').pipe(
      map(res => res.map((m: any) => new Terrain(m))),
      catchError(err => 
        {
          console.error(err);
          return [];
        })
    );
  }

//   public getOneByID(id: any): Observable<Terrain | null> 
//   {
//     return this.server.get<Terrain>('terrains/id/'+ id).pipe(
//       map(res => res.length > 0 ? new Terrain(res[0]) : null),
//       catchError(err => 
//         {
//           console.error(err);
//           return [];
//         })
//     );
//   }

//   public getOneByName(name: string): Observable<Terrain | null> 
//   {
//     return this.server.get<Terrain>('terrains/name/'+ name).pipe(
//       map(res => res.length > 0 ? new Terrain(res[0]) : null),
//       catchError(err => 
//         {
//           console.error(err);
//           return [];
//         })
//     );
//   }


  public addTerrain(terrain: Terrain):  Observable<Terrain[]> 
  {
    return this.server.post<Terrain>('terrains/create', terrain).pipe(
      map(res => res.map((m: any) => new Terrain(m))),
      catchError(err => 
        {
          console.error(err);
          return [];
        })
    );
  }

//   public updateTerrain(terrain: Terrain): Observable<Terrain | null>
//   {
//     return this.server.put<Terrain>('terrains/'+ terrain.Id_Terrain, terrain).pipe(
//       map(res => res.length > 0 ? new Terrain(res[0]) : null),
//       catchError(err => 
//         {
//           console.error(err);
//           return [];
//         })
//     );
//   }

//   public deleteTerrain(terrain: Terrain): Observable<Terrain[]>
//   {
//     return this.server.delete<Terrain>('terrains/'+ terrain.Id_Terrain).pipe(
//       map(res => res.map((m: any) => new Terrain(m))),
//       catchError(err => 
//         {
//           console.error(err);
//           return [];
//         })
//     );
//   }
}
