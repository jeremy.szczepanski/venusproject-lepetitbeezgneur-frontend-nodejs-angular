import { Injectable } from '@angular/core';
import { ServerService } from './server.service';
import { Commande } from '../models/commande.model';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private server: ServerService) 
  {}

  public getAll(): Observable<Commande[]> 
  {
    return this.server.get<Commande[]>('reservation/').pipe(
      map(res => res.map((m: any) => new Commande(m))),
      catchError(err => 
        {
          console.error(err);
          return [];
        })
    );
  }


  public getOneByID(id: any): Observable<Commande | null> 
  {
    return this.server.get<Commande>('reservation/id/'+ id).pipe(
      map(res => res.length > 0 ? new Commande(res[0]) : null),
      catchError(err => 
        {
          console.error(err);
          return [];
        })
    );
  }

  
  public addReservation(reservation: Commande):  Observable<Commande[]> 
  {
      return this.server.post<Commande>('reservation/create', reservation).pipe(
          map(res => res.map((m: any) => new Commande(m))),
          catchError(err => 
            {
                console.error(err);
                return [];
            })
            );
        }

//    public getOneByName(name: string): Observable<Commande| null> 
//    {
//      return this.server.get<Commande>('reservation/name/'+ name).pipe(
//        map(res => res.length > 0 ? new Commande(res[0]) : null),
//        catchError(err => 
//          {
//            console.error(err);
//            return [];
//          })
//      );
//    }


//   public updateReservation(commande: Commande): Observable<Commande | null>
//   {
//     return this.server.put<Commande>('reservation/'+ commande.Id_Commande, commande).pipe(
//       map(res => res.length > 0 ? new Commande(res[0]) : null),
//       catchError(err => 
//         {
//           console.error(err);
//           return [];
//         })
//     );
//   }

//   public deleteReservation(commande: Commande): Observable<Commande[]>
//   {
//     return this.server.delete<Commande>('reservation/'+ commande.Id_Commande).pipe(
//       map(res => res.map((m: any) => new Commande(m))),
//       catchError(err => 
//         {
//           console.error(err);
//           return [];
//         })
//     );
//   }



}
