import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//***MATERIAL MODULES ***/
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatRadioModule } from "@angular/material/radio";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { MatDatepickerModule } from '@angular/material/datepicker';

//***FULL CALENDAR MODULES ***/
import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!

import { ServerService } from './services/server.service';
import { AuthService } from './services/auth.service';
import { UsersService } from './services/user.service';
import { UsersCommonService } from './services/user-common.service';
import { AdminGuard, AuthGuard } from './services/auth-guard.service';

import { AppComponent } from './app.component';
import { TerrainsComponent } from './components/terrain/terrain.component';
import { UsersComponent } from './components/user/user.component';
import { ArticleComponent } from './components/article/article.component';
import { CommentaireComponent } from './components/commentaire/commentaire.component';
import { AuthComponent } from './components/auth/auth.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { FourhofourComponent } from './components/fourhofour/fourhofour.component';
import { MainComponent } from './components/main/main.component';
import { CreateOrderComponent } from './components/create-order/create-order.component';
//import { PaddleComponent } from './components/paddle/paddle.component';
//import { ProfileComponent } from './components/profile/profile.component';

export function tokenGetter() {
  return sessionStorage.getItem('id_token');
}

//***FULL CALENDAR***/
FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  interactionPlugin,
  dayGridPlugin
]);


@NgModule({
  declarations: [
    AppComponent,
    TerrainsComponent,
    UsersComponent,
    ArticleComponent,
    CommentaireComponent,
    AuthComponent,
    CreateUserComponent,
    EditUserComponent,
    FourhofourComponent,
    MainComponent,
    CreateOrderComponent,
    //PaddleComponent,
    //ProfileComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFormFieldModule, MatButtonModule, MatCheckboxModule, MatDialogModule, MatIconModule, MatInputModule, MatListModule, MatPaginatorModule,
    MatRadioModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatDatepickerModule,
    FullCalendarModule, // register FullCalendar with the app

    JwtModule.forRoot({
      config: {
          tokenGetter: tokenGetter,
          allowedDomains: ["localhost:8000"]
      }
  }),
    BrowserAnimationsModule
  ],

  providers: [
    ServerService,
    AuthService,
    UsersService,
    AuthGuard,
    AdminGuard,
    UsersCommonService
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
